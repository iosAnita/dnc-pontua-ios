//
//  String+Extension.swift
//  KitoPlastic
//
//  Created by macuser on 10/22/21.
//  Copyright © 2021 apple. All rights reserved.
//

import Foundation

extension String {
    func configUrl() -> URL? {
        let baseUrl = BaseURL.replacingOccurrences(of: "/api/", with: "")

        if self.contains(baseUrl) {
            return URL(string: self)
        } else {
			return URL(string: "\(baseUrl)\(self)")
        }
    }
}
