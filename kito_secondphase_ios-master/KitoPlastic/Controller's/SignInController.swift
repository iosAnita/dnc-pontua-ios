//
//  SignInController.swift
//  Conselho Fashion
//
//  Created by apple on 03/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import Alamofire

import IQKeyboardManagerSwift

// global variable

var mystateName = String()
var myCityName = String()
var mybrandName = String()
var userType = String()
var mylocationName = String()
var myBrandArray = [String]()
var MyBrandArray = [Any]()
var cpfMessage = ""
var datePicker = UIDatePicker()
@available(iOS 11.0, *)
class SignInController: UIViewController,UIGestureRecognizerDelegate,LBZSpinnerDelegate,UITextFieldDelegate{
    func spinnerChoose(_ spinner: LBZSpinner, index: Int, value: String) {
        if spinner == BrandlbzView{
            // mylocationid = chooselocation[index].id
            mybrandName = value
            BrandlbzView.text = value
            BrandlbzView.textColor = .darkGray
            chooseState() // API Call
            statelbzView.text = "ChooseState".localized(loc: "pt-BR")
            cityLbzView.text = "ChooseCity".localized(loc: "pt-BR")
            locationLbzView.text = "ChooseLocation".localized(loc: "pt-BR")
            //myBrandArray = chooselocation[index].brand
            // statelbzView.updateList(myBrandArray)
        }
        else if spinner == userTypeView{
            userType = value
            userTypeView.text = value
            userTypeView.textColor = .darkGray
            SingletonClass.sharedInstance.usertype = userType
        }
        else if spinner == statelbzView{
            mystateName = value
            statelbzView.text = value
            statelbzView.textColor = .darkGray
            chooseCity()
            cityLbzView.text = "ChooseCity".localized(loc: "pt-BR")
            locationLbzView.text = "ChooseLocation".localized(loc: "pt-BR")
        }
        else if spinner == cityLbzView{
            myCityName = value
            cityLbzView.text = value
            cityLbzView.textColor = .darkGray
            chooseLocation()
            locationLbzView.text = "ChooseLocation".localized(loc: "pt-BR")
        }
        else {
            mylocationName = value
            locationLbzView.text = value
            locationLbzView.textColor = .darkGray
            mylocationId = locationIDArray[index]
            MySelectedLocation = locationChecked[index]
            if userTypeView.text == "Vendedor"{
                EmailTopView.constant = 0
            }
            else{
                if MySelectedLocation == "0"{
                    
                    //Update Dictionary
                    LocationDict.updateValue(mybrandName, forKey: "Brand")
                    LocationDict.updateValue(mystateName, forKey: "State")
                    LocationDict.updateValue(myCityName, forKey: "City")
                    LocationDict.updateValue(mylocationName, forKey: "Location")
                    LocationDict.updateValue(mylocationId, forKey: "LocationID")
                    
                    
                    let newDictionary = LocationDict
                    
                    if !LocationArray.contains{ $0 == newDictionary } {
                        LocationArray.append(LocationDict)
                        print("my new Location array is:\(LocationArray)")
                        EmailTopView.constant = 75
                        locationCollectionView.delegate = self
                        locationCollectionView.dataSource = self
                        locationCollectionView.reloadData()
                    }
                    else{
                       print("your Current location is already exist in locationarray")
                    }
                }
                else{
                    locationLbzView.text = "ChooseLocation".localized(loc: mylanguagecode)
                    self.ShowAlertView(title: "Conselho Fashion", message: "Local já selecionado por outro usuário.", viewController: self)
                }
            }
         }
    }
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var view2: DesignableView!
    @IBOutlet weak var view1: DesignableView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var cpfTF: UITextField!
    @IBOutlet weak var contactTF: UITextField!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var brandTF: UITextField!
    @IBOutlet weak var eMailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var userNameLineView: UIView!
    @IBOutlet weak var cpfLineViw: UIView!
    @IBOutlet weak var contactLineView: UIView!
    @IBOutlet weak var locationlineView: UIView!
    @IBOutlet weak var citylineView: UIView!
    @IBOutlet weak var stateLineview: UIView!
    @IBOutlet weak var brandLineView: UIView!
    @IBOutlet weak var eMailLineView: UIView!
    @IBOutlet weak var passwordLineView: UIView!
    @IBOutlet weak var confirmPasswordLineView: UIView!
    @IBOutlet weak var facebookSignIN: UIButton!
    @IBOutlet weak var GoogleSignIn: UIButton!
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var RegisterBtn: UIButton!
    @IBOutlet weak var emailTFLogIN: UITextField!
    @IBOutlet weak var passwordTFLogin: UITextField!
    @IBOutlet weak var signUPView: UIView!
    
    @IBOutlet weak var passwordSeen: UIButton!
    @IBOutlet weak var confirmPaswordSeen: UIButton!
    @IBOutlet weak var BrandlbzView: LBZSpinner!
    
    @IBOutlet weak var SignupView1Bottom: UIView!
    @IBOutlet weak var signUpView2Bottom: UIView!
    @IBOutlet weak var signInBottom: UIView!
    
    
    @IBOutlet weak var cityLbzView: LBZSpinner!
    @IBOutlet weak var locationLbzView: LBZSpinner!
    @IBOutlet weak var statelbzView: LBZSpinner!
    @IBOutlet weak var EmailorCpfText: UILabel!
    @IBOutlet weak var passwordText: UILabel!
    @IBOutlet weak var ForgotBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var orloginWith: UILabel!
    @IBOutlet weak var DonthaveLogin: UILabel!
    @IBOutlet weak var sellerCodeTextField: UITextField!
    
    @IBOutlet weak var BrandText: UILabel!
    @IBOutlet weak var stateText: UILabel!
    @IBOutlet weak var cityText: UILabel!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var EmailText: UILabel!
    @IBOutlet weak var SignupPasswordText: UILabel!
    @IBOutlet weak var confirmPaswordText: UILabel!
    @IBOutlet weak var SignupBtn: UIButton!
    @IBOutlet weak var usernameText: UILabel!
    @IBOutlet weak var cpfText: UILabel!
    @IBOutlet weak var contactText: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var alreadySignupView1Btn: UILabel!
    @IBOutlet weak var alreadySignupView2Btn: UILabel!
    @IBOutlet weak var byloginglbl: UILabel!
    @IBOutlet weak var loginTerms: UIButton!
    @IBOutlet weak var loginAnd: UILabel!
    @IBOutlet weak var loginPrivacy: UIButton!
    @IBOutlet weak var bySigingView1: UILabel!
    @IBOutlet weak var view1terms: UIButton!
    @IBOutlet weak var view1Privacy: UIButton!
    @IBOutlet weak var view1and: UILabel!
    @IBOutlet weak var bysigingView2: UILabel!
    @IBOutlet weak var view2terms: UIButton!
    @IBOutlet weak var view2and: UILabel!
    @IBOutlet weak var view2Privacy: UIButton!
    @IBOutlet weak var scroolView: UIScrollView!
    @IBOutlet weak var CountryCodeTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var phonenumberlineView: UIView!
    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var userTypeTF: UITextField!
    @IBOutlet weak var userTypeView: LBZSpinner!
    @IBOutlet weak var userTypeLineView: UIView!
    @IBOutlet weak var locationCollectionView: UICollectionView!
    @IBOutlet weak var EmailTopView: NSLayoutConstraint!
    @IBOutlet weak var genderView: UIView!
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var confirmEmailTxt: UITextField!
    @IBOutlet weak var dobtxt: UITextField!
    @IBOutlet weak var dobLbl: UILabel!
    
    // local variables
    var myImage = NSData()
    var passwordSeenIcon = true
    var ConfirmPasswordSeen = true
    var imagePicker:UIImagePickerController?=UIImagePickerController()
    var MyImage : UIImage!
    var tapgesture = UITapGestureRecognizer()
    var tapgesture1 = UITapGestureRecognizer()
    var tapgesture2 = UITapGestureRecognizer()
    var chooselocation = [Location]()  // variable declare of location struct type
    var BrandValue = [BrandName]()  // variable declare of BrandName struct type
    var name = [String]()
    var State = [String]()
    var City = [String]()
    var location = [String]()
    var brandName = [String]()
    var locationIDArray = [String]()
    var locationChecked = [String]()
    var mylocationId = ""
    var MySelectedLocation = ""
    var selected_gender = "M"
    var LocationArray = [[String:String]]()
    var LocationDict = ["Brand":"",
                        "State":"",
                        "City":"",
                        "Location":"",
                        "LocationID":""]
    var usertype = "1"
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTF.delegate = self
        cpfTF.delegate = self
        
        verifyBtn.isHidden = true
        SignupView1Bottom.isHidden = true
        signUpView2Bottom.isHidden = true
        scroolView.isScrollEnabled = false
        //  facebookSignIN.isUserInteractionEnabled = false
        //  GoogleSignIn.isUserInteractionEnabled = false
        brandTF.delegate = self
        BrandlbzView.delegate = self
        statelbzView.delegate = self
        cityLbzView.delegate = self
        locationLbzView.delegate = self
        BrandlbzView.lineColor = .clear
        statelbzView.lineColor = .clear
        userTypeView.delegate = self
        userTypeView.lineColor = .clear
        showDatePicker()
        
        ChooseBrand()  //Api call
        if BrandlbzView.selectedIndex == LBZSpinner.INDEX_NOTHING {
            
            BrandlbzView.text = "ChooseBrand".localized(loc: mylanguagecode)
        }
        if  statelbzView.selectedIndex == LBZSpinner.INDEX_NOTHING{
            statelbzView.text = "ChooseState".localized(loc: mylanguagecode)
        }
        if cityLbzView.selectedIndex == LBZSpinner.INDEX_NOTHING {
            
            cityLbzView.text = "ChooseCity".localized(loc: mylanguagecode)
        }
        if locationLbzView.selectedIndex == LBZSpinner.INDEX_NOTHING {
            
            locationLbzView.text = "ChooseLocation".localized(loc: mylanguagecode)
        }
        if userTypeView.selectedIndex == LBZSpinner.INDEX_NOTHING{
            userTypeView.text = "escolha o tipo de usuário"
        }
        
        
        tapgesture.delegate = self
        
        signUPView.isHidden = true
        backBtn.isHidden = true
        
        // Do any additional setup after loading the view.
        logInBtn.setTitleColor(UIColor.white, for: .normal)
        RegisterBtn.setTitleColor(FONT_COLOR, for: .normal)
        
        // check user id for login or already login
        if  UserDefaults.standard.value(forKey: "userid") as? String  != nil{
            performPushSeguefromController(identifier: "TabBarController")
        }
        else {
            let userId = UserDefaults.standard.string(forKey: "id") ?? "could not find any id"
            print(userId)
        }
        
        //change placeholder textfield colour
        cpfTF.attributedPlaceholder = NSAttributedString(string: "___.___.___-__",attributes:[NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        //user type list updated
        userTypeView.updateList(UserTypeArray)
    }
    //MARK: Action Sheet to open camera and gallery
//    func genderActionSheet(){
//        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
//        let maleAction = UIAlertAction(title: "masculino", style: .default, handler: {
//            (alert: UIAlertAction!) -> Void in
//            self.genderTxt.text = "masculino"
//            self.selected_gender = "1"
//        })
//        let femaleAction = UIAlertAction(title: "feminino", style: .default, handler: {
//            (alert: UIAlertAction!) -> Void in
//            self.genderTxt.text = "feminino"
//            self.selected_gender = "2"
//        })
//        let otherAction = UIAlertAction(title: "outras", style: .default, handler: {
//            (alert: UIAlertAction!) -> Void in
//            self.genderTxt.text = "outras"
//            self.selected_gender = "3"
//        })
//        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel, handler: {
//            (alert: UIAlertAction!) -> Void in
//
//        })
//        optionMenu.addAction(maleAction)
//        optionMenu.addAction(femaleAction)
//        optionMenu.addAction(otherAction)
//        optionMenu.addAction(cancelAction)
//        self.present(optionMenu, animated: true, completion: nil)
//    }
//    @IBAction func genderbtn(_ sender: UIButton) {
//        genderActionSheet()
//    }
    
    @IBAction func ChooseDate(_ sender: UITextField) {
        showDatePicker()
    }
    func showDatePicker(){
           //Formate Date
           datePicker.datePickerMode = .date
           datePicker.maximumDate = Date()
           //ToolBar
           let toolbar = UIToolbar();
           toolbar.sizeToFit()
           let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
           let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
           toolbar.tintColor = THEME_COLOR
           toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
           
           dobtxt.inputAccessoryView = toolbar
           dobtxt.inputView = datePicker
           
       }
       
       @objc func donedatePicker(){
           let formatter = DateFormatter()
           formatter.dateFormat = "dd/MM/yyyy"
           dobtxt.text = formatter.string(from: datePicker.date)
           self.view.endEditing(true)
       }
       
       @objc func cancelDatePicker(){
           self.view.endEditing(true)
       }
    
    override func viewWillAppear(_ animated: Bool) {
        changeStoryboardWithLanguage()
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.isNavigationBarHidden = true
    }
    //MARK:- Textfield delegate method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTF{
            let quest:String = "+55" // Character to append
            let Hiphen : Character = "-"
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                print("Backspace was pressed")
            }
            
            if phoneNumberTF.text?.count == 0 {
                if (isBackSpace == -92) {
                    print("Backspace was pressed")
                }
                else{
                    self.phoneNumberTF.text!.append(quest) // append "+"
                }
                
            }
            if phoneNumberTF.text?.count == 6 { // if textfield has exactly one character
                if (isBackSpace == -92) {
                    print("Backspace was pressed")
                }
                else{
                    self.phoneNumberTF.text!.append(Hiphen) // append "+"
                }
            }
            if phoneNumberTF.text?.count == 12 { // if textfield has exactly one character
                if (isBackSpace == -92) {
                    print("Backspace was pressed")
                }
                else{
                    self.phoneNumberTF.text!.append(Hiphen) // append "+"
                }
            }
            let currentCharacterCount = phoneNumberTF.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 17
        } else if textField == confirmEmailTxt {}
        else {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            let Hiphen : Character = "-"
            let dot : Character = "."
            if cpfTF.text?.count == 3 {
                if (isBackSpace == -92) {
                    print("Backspace was pressed")
                }
                else{
                    self.cpfTF.text!.append(dot) // append "+"
                }
            }
            if cpfTF.text?.count == 7 {
                if (isBackSpace == -92) {
                    print("Backspace was pressed")
                }
                else{
                    self.cpfTF.text!.append(dot) // append "+"
                }
            }
            if cpfTF.text?.count == 11{
                if (isBackSpace == -92) {
                    print("Backspace was pressed")
                }
                else{
                    self.cpfTF.text!.append(Hiphen) // append "+"
                }
            }
            let currentCharacterCount = cpfTF.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 14
        }
        
        return true
    }
    //MARK:- language change
    func changeStoryboardWithLanguage(){
        EmailorCpfText.text = "EmailCPF".localized(loc: mylanguagecode)
        DonthaveLogin.text = "DontAccount".localized(loc: mylanguagecode)
        emailTFLogIN.placeholder = "EmailCPF".localized(loc: mylanguagecode)
        passwordTFLogin.placeholder = "Password".localized(loc: mylanguagecode)
        passwordText.text = "Password".localized(loc: mylanguagecode)
        
        ForgotBtn.setTitle("ForgotPassword".localized(loc: mylanguagecode), for: .normal)
        signInBtn.setTitle("Signin".localized(loc: mylanguagecode), for: .normal)
        RegisterBtn.setTitle("cadastre-se", for: .normal)
        logInBtn.setTitle("entrar", for: .normal)
        usernameText.text = "userName".localized(loc: mylanguagecode)
        cpfText.text = "CPF".localized(loc: mylanguagecode)
        //contactText.text = "Código do país"
        
        userNameTF.placeholder = "userName".localized(loc: mylanguagecode)
        // cpfTF.placeholder = "CPF".localized(loc: mylanguagecode)
        //contactTF.placeholder = "contact".localized(loc: mylanguagecode)
        nextBtn.setTitle("Next".localized(loc: mylanguagecode), for: .normal)
        alreadySignupView1Btn.text = "Alreadyaccount".localized(loc: mylanguagecode)
        BrandText.text = "Brand".localized(loc: mylanguagecode)
        stateText.text = "State".localized(loc: mylanguagecode)
        cityText.text = "City".localized(loc: mylanguagecode)
        locationText.text = "Location".localized(loc: mylanguagecode)
        EmailText.text = "Email".localized(loc: mylanguagecode)
        passwordText.text = "Password".localized(loc: mylanguagecode)
        confirmPaswordText.text = "Confirm".localized(loc: mylanguagecode)
        eMailTF.placeholder = "Email".localized(loc: mylanguagecode)
        passwordTF.placeholder = "Password".localized(loc: mylanguagecode)
        confirmPasswordTF.placeholder = "Confirm".localized(loc: mylanguagecode)
        SignupBtn.setTitle("SignUp".localized(loc: mylanguagecode), for: .normal)
        alreadySignupView2Btn.text = "Alreadyaccount".localized(loc: mylanguagecode)
        
        byloginglbl.text = "byLogingUP".localized(loc: mylanguagecode)
        bySigingView1.text = "bySigingup".localized(loc: mylanguagecode)
        bysigingView2.text = "bySigingup".localized(loc: mylanguagecode)
        loginAnd.text = "andthe".localized(loc: mylanguagecode)
        view1and.text = "andthe".localized(loc: mylanguagecode)
        view2and.text = "andthe".localized(loc: mylanguagecode)
        loginTerms.setTitle("Terms".localized(loc: mylanguagecode), for: .normal)
        view1terms.setTitle("Terms".localized(loc: mylanguagecode), for: .normal)
        view2terms.setTitle("Terms".localized(loc: mylanguagecode), for: .normal)
        loginPrivacy.setTitle("Privacy".localized(loc: mylanguagecode), for: .normal)
        view1Privacy.setTitle("Privacy".localized(loc: mylanguagecode), for: .normal)
        view2Privacy.setTitle("Privacy".localized(loc: mylanguagecode), for: .normal)
    }
    
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == cpfTF{
            print("Editing End")
            validCPF() //API Call
        } else if textField == confirmEmailTxt {
            if eMailTF.text != confirmEmailTxt.text {
                ShowAlertView(title: "confirmar e-mail", message: "Seu e-mail não corresponde, digite o endereço de e-mail correto", viewController: self)
            }
        }
    }
    
    @IBAction func forgetPassworTap(_ sender: UIButton) {
        performPushSeguefromController(identifier: "ForgotPasswordController")
    }
    
    @IBAction func termsBtnTap(_ sender: UIButton) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionController") as! Terms_ConditionController
        
        secondViewController.titleName = "Terms".localized(loc: mylanguagecode)
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func privacyBtnTap(_ sender: UIButton) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "Terms_ConditionController") as! Terms_ConditionController
        
        
        secondViewController.titleName = "Privacy".localized(loc: mylanguagecode)
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    
    @IBAction func register(_ sender: UIButton) {
        
        self.view.endEditing(true)
        scroolView.isScrollEnabled = true
        if signUPView.isHidden == true{
            signUPView.isHidden = false
            backBtn.isHidden = true
            view1.isHidden = false
            view2.isHidden = true
            signInBottom.isHidden = true
            signUpView2Bottom.isHidden = true
            SignupView1Bottom.isHidden = false
        }
        GlobalClass.ischanged = 3
        RegisterBtn.setTitleColor(UIColor.white, for: .normal)
        logInBtn.setTitleColor(FONT_COLOR, for: .normal)
    }
    
    @IBAction func passwordSeenTap(_ sender: UIButton) {
        if passwordSeenIcon == true{
            passwordTF.isSecureTextEntry = false
            passwordSeen.setImage(#imageLiteral(resourceName: "seenPassword"), for: .normal)
        }
        else{
            passwordSeen.setImage(#imageLiteral(resourceName: "unseenPassword"), for: .normal)
            passwordTF.isSecureTextEntry = true
        }
        passwordSeenIcon = !passwordSeenIcon
    }
    
    @IBAction func ConfirmPasswordSeen(_ sender: UIButton) {
        if ConfirmPasswordSeen == true{
            confirmPasswordTF.isSecureTextEntry = false
            confirmPaswordSeen.setImage(#imageLiteral(resourceName: "seenPassword"), for: .normal)
        }
        else{
            confirmPaswordSeen.setImage(#imageLiteral(resourceName: "unseenPassword"), for: .normal)
            confirmPasswordTF.isSecureTextEntry = true
        }
        ConfirmPasswordSeen = !ConfirmPasswordSeen
    }
    
    @IBAction func DontAccountTap(_ sender: UIButton) {
        scroolView.isScrollEnabled = true
        signUPView.isHidden = false
        backBtn.isHidden = true
        view1.isHidden = false
        view2.isHidden = true
        signInBottom.isHidden = true
        signUpView2Bottom.isHidden = true
        SignupView1Bottom.isHidden = false
        RegisterBtn.setTitleColor(UIColor.white, for: .normal)
        logInBtn.setTitleColor(FONT_COLOR, for: .normal)
    }
    
    @IBAction func alreadyAccountView1Tap(_ sender: UIButton) {
        scroolView.isScrollEnabled = false
        self.view.endEditing(true)
        signUPView.isHidden = true
        backBtn.isHidden = true
        logInBtn.setTitleColor(UIColor.white, for: .normal)
        RegisterBtn.setTitleColor(FONT_COLOR, for: .normal)
        SignupView1Bottom.isHidden = true
        signUpView2Bottom.isHidden = true
        signInBottom.isHidden = false
    }
    
    @IBAction func alreadyAccountView2Tap(_ sender: UIButton) {
        scroolView.isScrollEnabled = false
        self.view.endEditing(true)
        signUPView.isHidden = true
        backBtn.isHidden = true
        logInBtn.setTitleColor(UIColor.white, for: .normal)
        RegisterBtn.setTitleColor(FONT_COLOR, for: .normal)
        SignupView1Bottom.isHidden = true
        signUpView2Bottom.isHidden = true
        signInBottom.isHidden = false
    }
    
    @IBAction func logIn(_ sender: UIButton) {
        self.view.endEditing(true)
        scroolView.isScrollEnabled = false
        signUPView.isHidden = true
        backBtn.isHidden = true
        logInBtn.setTitleColor(UIColor.white, for: .normal)
        RegisterBtn.setTitleColor(FONT_COLOR, for: .normal)
        SignupView1Bottom.isHidden = true
        signUpView2Bottom.isHidden = true
        signInBottom.isHidden = false
    }
    
    
    // MARK:- Sign in Button Tapped
    @IBAction func signIN(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if (emailTFLogIN.text == ""){
            ShowAlertView(title: "Conselho Fashion", message: "EmailAlert".localized(loc: mylanguagecode), viewController: self)
        }
            //         else if (isValidEmail1(testStr: emailTFLogIN.text!) == false){
            //            ShowAlertView(title: "Conselho Fashion", message: "email must be in valid form", viewController: self)
            //        }
        else if (passwordTFLogin.text == ""){
            ShowAlertView(title: "Conselho Fashion", message: "PasswordAlert".localized(loc: mylanguagecode), viewController: self)
        }
            //        else if ((passwordTFLogin.text?.count)! < 7){
            //            ShowAlertView(title: "Conselho Fashion", message: "PasswordGreaterAlert".localized(loc: mylanguagecode), viewController: self)
            //        }
        else {
            logINApi()  // loginApi Function Call
        }
    }
    
    //MARK:- ValidCPFNumber
    
    func validCPF(){
        let parameter : [String: String] = ["cpf" : cpfTF.text!]
        print(parameter)
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.validCPF.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool  == true {
                cpfMessage = "valid"
                self.verifyBtn.isHidden = false
                self.cpfLineViw.backgroundColor = Line_View_colour
            }
            else{
                cpfMessage = "invalid"
                self.verifyBtn.isHidden = true
                self.cpfLineViw.backgroundColor = UIColor.red
                self.signUPView.isHidden = false
                self.backBtn.isHidden = true
                self.view1.isHidden = false
                self.view2.isHidden = true
                self.signInBottom.isHidden = true
                self.signUpView2Bottom.isHidden = true
                self.SignupView1Bottom.isHidden = false
                self.ShowAlertView(title: "Conselho Fashion", message: "seu número de CPF é inválido, preencha novamente.", viewController: self)
                
            }
        }
        
    }
    
    
    //--------------------- MARK:- Register LogIN Data on Server With Alarmofire-------------------
    func logINApi(){
        showProgress()
        let parameter = ["email" : emailTFLogIN.text!,
                         "password" : passwordTFLogin.text!,
                         "device_type" : "ios",
                         "device_token" : (UserDefaults.standard.value(forKey: "FCM_TOKEN") as! String),
                         "fcmtoken" : (UserDefaults.standard.value(forKey: "FCM_TOKEN") as! String)
        ]
        print(parameter)
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.userSignIn.caseValue, parameter: parameter ) { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool  == true {
                if let data = dic.value(forKey: "data") as? NSDictionary{
                    print (data)
                    
                    // get user id nd save it
                    let userId = (data.value(forKey: "id") as! NSNumber)
                    UserDefaults.standard.set("\(userId)", forKey: "userid")
                    
                    // get token of user and save it
                    let userToken = (dic.value(forKey: "token")) as! String
                    UserDefaults.standard.set("\(userToken)", forKey: "usertoken")
                    
                    self.performPushSeguefromController(identifier: "TabBarController")
                }
            }
            else{
                self.hideProgress()
                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    
    
    //MARK:- =========================Choose Brands on Server API==================
    func ChooseBrand(){
        networkServices.shared.postDatawithoutHeaderWithoutParameter(methodName: methodsName.userCase.ChooseBrand.caseValue) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    self.name.removeAll()
                    for i in 0..<data.count{
                        self.name.append((data[i] as AnyObject).value(forKey: "brand") as! String)
                    }
                    self.BrandlbzView.updateList(self.name)
                    print(self.name)
                }
            }
            else{
                //                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    
    //MARK:- =========================choose State on Server API==================
    func chooseState(){
        let parameter = ["brand" : mybrandName]
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.GetallState.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    self.State.removeAll()
                    for i in 0..<data.count{
                        self.State.append((data[i] as AnyObject).value(forKey: "state") as! String)
                    }
                    self.statelbzView.updateList(self.State)
                }
            }
            else{
                //                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    //MARK:- =========================Choose City on Server API==================
    func chooseCity(){
        let parameter = ["state" : mystateName,
                         "brand" : mybrandName]
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.GetallCity.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    self.City.removeAll()
                    for i in 0..<data.count{
                        self.City.append((data[i] as AnyObject).value(forKey: "city") as! String)
                        
                    }
                    self.cityLbzView.updateList(self.City)
                }
            }
            else{
                //                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    //MARK:- =========================Choose City on Server API==================
    func chooseLocation(){
       
//        if userTypeView.text == "Vendedor"{
//            usertype = "1"
//        }
//        else if userTypeView.text == "Comprador"{
//            usertype = "2"
//        }
//        else if userTypeView.text == "Gerente"{
//            usertype = "3"
//        }
        let parameter = ["city" : myCityName,
                         "brand" : mybrandName,
                         "user_type" : usertype]
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.GetallLocation.caseValue, parameter: parameter) { (response) in
            print(response)
            let dic = response as! NSDictionary
            print(dic)
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                    print(data)
                    self.location.removeAll()
                    self.locationIDArray.removeAll()
                    self.locationChecked.removeAll()
                    for i in 0..<data.count{
                        self.location.append((data[i] as AnyObject).value(forKey: "name") as! String)
                        self.locationIDArray.append("\((data[i] as AnyObject).value(forKey: "id") as! NSNumber)")
                        self.locationChecked.append((data[i] as AnyObject).value(forKey: "status") as! String)
                    }
                    self.locationLbzView.updateList(self.location)
                }
            }
            else{
                //                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    
    
    //MARK:- perform action when user tap  Location cross Button Tap
    @IBAction func crossBtnTap(_ sender: UIButton) {
        
        let buttonPostion = sender.convert(sender.bounds.origin, to: locationCollectionView)
        
        if let indexPath = locationCollectionView.indexPathForItem(at: buttonPostion) {
            let rowIndex =  indexPath.row
            print("Button Tapped at indexpath:- \(rowIndex)")
            let ArrayDict : [[String:String]] = [LocationArray.remove(at: rowIndex)]
            print(ArrayDict)
            locationCollectionView.reloadData()
            if LocationArray.count == 0{
                EmailTopView.constant = 0
                BrandlbzView.text = "ChooseBrand".localized(loc: "pt-BR")
                statelbzView.text = "ChooseState".localized(loc: "pt-BR")
                cityLbzView.text = "ChooseCity".localized(loc: "pt-BR")
                locationLbzView.text = "ChooseLocation".localized(loc: "pt-BR")
            }
            else{
                EmailTopView.constant = 75
            }
            
        }
        
        
    }
    
}
//=====================MARK:-Register view Buttons, textField and API=============================

@available(iOS 11.0, *)
extension SignInController: UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    @IBAction func BackBtnTap(_ sender: UIButton) {
        self.view.endEditing(true)
        view2.isHidden = true
        view1.isHidden = false
        backBtn.isHidden = true
        SignupView1Bottom.isHidden = false
        signUpView2Bottom.isHidden = true
        signInBottom.isHidden = true
    }
    @IBAction func Next(_ sender: UIButton) {
        self.view.endEditing(true)
        // check whether cpf number is Correct or not
        validCPF()
        if (userNameTF.text == ""){
            ShowAlertView(title: "Conselho Fashion", message: "usernameAlert".localized(loc: mylanguagecode), viewController: self)
            userNameLineView.backgroundColor = UIColor.red
        }
//        else if userType == ""{
//            ShowAlertView(title: "Conselho Fashion", message: "Escolha primeiro o tipo de usuário", viewController: self)
//            userTypeLineView.backgroundColor = .red
//            cpfLineViw.backgroundColor = Line_View_colour
//            userNameLineView.backgroundColor = Line_View_colour
//            phonenumberlineView.backgroundColor = Line_View_colour
//        }
        else if (cpfTF.text == ""){
            ShowAlertView(title: "Conselho Fashion", message: "CpfAlert".localized(loc: mylanguagecode), viewController: self)
            
            cpfLineViw.backgroundColor = UIColor.red
            userNameLineView.backgroundColor = Line_View_colour
            phonenumberlineView.backgroundColor = Line_View_colour
        }
            
        else if (phoneNumberTF.text == ""){
            ShowAlertView(title: "Conselho Fashion", message: "ContactAlert".localized(loc: mylanguagecode), viewController: self)
            userNameLineView.backgroundColor = Line_View_colour
            cpfLineViw.backgroundColor = Line_View_colour
            phonenumberlineView.backgroundColor = UIColor.red
        }
        else if cpfMessage == "invalid"{
            self.ShowAlertView(title: "Conselho Fashion", message: "seu número de CPF é inválido, preencha novamente.", viewController: self)
            cpfLineViw.backgroundColor = UIColor.red
            userNameLineView.backgroundColor = Line_View_colour
            phonenumberlineView.backgroundColor = Line_View_colour
        } else if sellerCodeTextField.text == "" {
            ShowAlertView(title: "Conselho Fashion", message: "sellerCode".localized(loc: mylanguagecode), viewController: self)
        }
        else{
            // contactLineView.backgroundColor = Line_View_colour
            cpfLineViw.backgroundColor = Line_View_colour
            userNameLineView.backgroundColor = Line_View_colour
            phonenumberlineView.backgroundColor = Line_View_colour
            view1.isHidden = true
            view2.isHidden = false
            SignupView1Bottom.isHidden = true
            signUpView2Bottom.isHidden = false
            backBtn.isHidden = false
        }
        
    }
    @IBAction func SignUp(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if (BrandlbzView.text == "ChooseBrand".localized(loc: "pt-BR")){
            ShowAlertView(title: "Conselho Fashion", message: "BrandAlert".localized(loc: mylanguagecode), viewController: self)
            brandLineView.backgroundColor = UIColor.red
        }
        else if (statelbzView.text == "ChooseState".localized(loc: "pt-BR")){
            ShowAlertView(title: "Conselho Fashion", message: "stateAlert".localized(loc: mylanguagecode), viewController: self)
            stateLineview.backgroundColor = UIColor.red
            brandLineView.backgroundColor = Line_View_colour
            
        }
        else if (cityLbzView.text == "ChooseCity".localized(loc: "pt-BR")){
            ShowAlertView(title: "Conselho Fashion", message: "CityAlert".localized(loc: mylanguagecode), viewController: self)
            citylineView.backgroundColor = UIColor.red
            stateLineview.backgroundColor = Line_View_colour
            brandLineView.backgroundColor = Line_View_colour
        }else if (locationLbzView.text == "ChooseLocation".localized(loc: "pt-BR")){
            ShowAlertView(title: "Conselho Fashion", message: "LocationAlert".localized(loc: mylanguagecode), viewController: self)
            
            locationlineView.backgroundColor = UIColor.red
            citylineView.backgroundColor = Line_View_colour
            brandLineView.backgroundColor = Line_View_colour
            stateLineview.backgroundColor = Line_View_colour
        }
        else if (eMailTF.text == ""){
            
            ShowAlertView(title: "Conselho Fashion", message: "EmailAlert".localized(loc: mylanguagecode), viewController: self)
            eMailLineView.backgroundColor = UIColor.red
            brandLineView.backgroundColor = Line_View_colour
            locationlineView.backgroundColor = Line_View_colour
            citylineView.backgroundColor = Line_View_colour
            stateLineview.backgroundColor = Line_View_colour
            
        }
        else if (isValidEmail1(testStr: eMailTF.text! ) == false){
            ShowAlertView(title: "Conselho Fashion", message: "mustValid".localized(loc: mylanguagecode), viewController: self)
        }
        else if (passwordTF.text == ""){
            ShowAlertView(title: "Conselho Fashion", message: "PasswordAlert".localized(loc: mylanguagecode), viewController: self)
            passwordLineView.backgroundColor = UIColor.red
            eMailLineView.backgroundColor = Line_View_colour
            brandLineView.backgroundColor = Line_View_colour
            locationlineView.backgroundColor = Line_View_colour
            citylineView.backgroundColor = Line_View_colour
            stateLineview.backgroundColor = Line_View_colour
        }
        else if ((passwordTF.text?.count)! < 7){
            ShowAlertView(title: "Conselho Fashion", message: "PasswordGreaterAlert".localized(loc: mylanguagecode), viewController: self)
        }
        else if (confirmPasswordTF.text == ""){
            ShowAlertView(title: "Conselho Fashion", message: "ConfirmAlert".localized(loc: mylanguagecode), viewController: self)
            confirmPasswordLineView.backgroundColor = UIColor.red
            passwordLineView.backgroundColor = Line_View_colour
            eMailLineView.backgroundColor = Line_View_colour
            brandLineView.backgroundColor = Line_View_colour
            locationlineView.backgroundColor = Line_View_colour
            citylineView.backgroundColor = Line_View_colour
            stateLineview.backgroundColor = Line_View_colour
        }
        else if (confirmPasswordTF.text != passwordTF.text){
            ShowAlertView(title: "Conselho Fashion", message: "equalAlert".localized(loc: mylanguagecode), viewController: self)
        }
            
        else{
            RegisterApi() // Register API function call
            
        }}
    
    //MARK: Email Format Validation
    func isValidEmail1(testStr:String) -> Bool
        
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr)
        
    }
    
    @IBAction func openCamera(_ sender: UIButton) {
        CameraActionSheet()
    }
    
    
    //MARK: Action Sheet to open camera and gallery
    func CameraActionSheet(){
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let TakeAction = UIAlertAction(title: "TakePhoto".localized(loc: mylanguagecode), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.opencamera()
        })
        let ChooseAction = UIAlertAction(title: "ChoosePhoto".localized(loc: mylanguagecode), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openGallery()
        })
        let cancelAction = UIAlertAction(title: "Cancel".localized(loc: mylanguagecode), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        // Add the actions
        imagePicker?.delegate = self
        optionMenu.addAction(TakeAction)
        optionMenu.addAction(ChooseAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK: Function to open Camera
    func opencamera()
    {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker!.delegate = self
            imagePicker!.sourceType = UIImagePickerController.SourceType.camera
            imagePicker!.allowsEditing = true
            imagePicker!.cameraCaptureMode = UIImagePickerController.CameraCaptureMode.photo;
            self.present(imagePicker!, animated: true, completion: nil)
        }else{
            ShowAlertView(title: "Conselho Fashion", message: "problemCamera".localized(loc: mylanguagecode), viewController: self)
        }
    }
    //MARK: Function to open Gallery
    
    func openGallery()
    {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            imagePicker!.delegate = self
            imagePicker!.sourceType = UIImagePickerController.SourceType.photoLibrary;
            imagePicker!.allowsEditing = true
            self.present(imagePicker!, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        print(image.size)
        userImage.image = image
        MyImage = image
        userImage.layer.cornerRadius = userImage.frame.size.height/2
        userImage.clipsToBounds = true
        
        dismiss(animated: true)
    }
    
    // MARK:- Register Data on Server With Alarmofire without profile pic
    func RegisterApi(){
        showProgress()
        //Get device Token value
        var parameter : [String : Any]
        var usertype = ""
        let DeviceToken =  UserDefaults.standard.string(forKey: "DeviceToken")
        let ContactNumber = phoneNumberTF.text!
        let NewCpf = String(describing: (cpfTF.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())!)
//        if userTypeView.text == "Vendedor"{
//            parameter  = ["email": eMailTF.text!,
//                          "username" : userNameTF.text!,
//                          "phone_number" : ContactNumber,
//                          "address" : "\(mylocationId)",
//                "brand" : "\(mybrandName)",
//                "state" : "\(mystateName)",
//                "city" :  "\(myCityName)",
//                "password" : passwordTF.text!,
//                "profile_pic" : "",
//                "cpf_number": NewCpf,
//                "device_type" : "ios",
//                "device_token" : DeviceToken!,
//                "fcmtoken" : UserDefaults.standard.value(forKey: "FCM_TOKEN")as! String
//            ]
//
//        }
//        else{
//            if userTypeView.text == "Comprador"{
//                usertype = "2"
//            }
//            else if userTypeView.text == "Gerente"{
//                usertype = "3"
//            }
//            parameter  = ["email": eMailTF.text!,
//                          "username" : userNameTF.text!,
//                          "phone_number" : ContactNumber,
//                          "user_type" : usertype,
//                          "user_Location" : LocationArray,
//                          "password" : passwordTF.text!,
//                          "profile_pic" : "",
//                          "cpf_number": NewCpf,
//                          "device_type" : "ios",
//                          "device_token" : DeviceToken!,
//                          "fcmtoken" : UserDefaults.standard.value(forKey: "FCM_TOKEN")as! String]
//        }
        parameter  = ["email": eMailTF.text!,
                        "username" : userNameTF.text!,
                        "phone_number" : ContactNumber,
                        "address" : "\(mylocationId)",
                        "brand" : "\(mybrandName)",
                        "state" : "\(mystateName)",
                        "city" :  "\(myCityName)",
                        "password" : passwordTF.text!,
                        "profile_pic" : "",
                        "cpf_number": NewCpf,
                        "device_type" : "ios",
                        "device_token" : DeviceToken!,
                        "fcmtoken" : UserDefaults.standard.value(forKey: "FCM_TOKEN")as! String,
                        "user_type" : "1",
                        "dob": self.dobtxt.text!,
                        "gender": selected_gender,
                        "vendor_code": sellerCodeTextField.text!
                    ]
        print(parameter)
        networkServices.shared.postDatawithoutHeader(methodName: methodsName.userCase.userSignUp.caseValue, parameter: parameter )   { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            print (dic)
            if dic.value(forKey: "success") as! Bool == true
            {
                if let data = dic.value(forKey: "data") as? NSDictionary{
                    print(data)
                    // Access API Token and save it
                    let token = dic.value(forKey: "token")
                    UserDefaults.standard.set(token, forKey: "usertoken")
                    // get user id nd save it
                    let userId = (data.value(forKey: "id") as! NSNumber)
                    UserDefaults.standard.set("\(userId)", forKey: "userid")
                    if self.MyImage == nil{
                        let GoHome = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                        self.navigationController?.pushViewController(GoHome, animated: true)
                    }
                    else{
                        self.UploadImage1()
                    }
                    
                }
                
            }
            else{
                self.hideProgress()
                self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
            }
        }
    }
    
    // MARK :-====================== Upload image on server=============================
    func UploadImage1(){
        showProgress()
        let image = MyImage
        let myImgData = image!.jpegData(compressionQuality: 0.75)
        
        let methodname = methodsName.userCase.uploadImage.caseValue
        let userID = UserDefaults.standard.value(forKey: "userid") as! String
        networkServices.shared.updateMultipart(action: methodname, param: ["userid":userID],imageData: myImgData!, success: { (response) in
            print(response)
            let dict:[String:Any] = response as! [String : Any]
            if let sucess = dict["success"] as? Bool{
                if sucess == true{
                    let GoHome = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                    self.navigationController?.pushViewController(GoHome, animated: true)
                }else{
                    if let message = dict["message"] as? String{
                        self.ShowAlertView(title: "Conselho Fashion", message: message, viewController: self)
                    }
                }
            }
        }) { (error) in
            self.hideProgress()
            print(error)
            self.ShowAlertView(title: "Conselho Fashion", message: error.localizedDescription, viewController: self)
        }
        
    }
    
    
    // MARK :- Upload image on server with Alarmofire
    func uploadImage(){
        showProgress()
        var parameter : [String : Any]
        var usertype = ""
        let DeviceToken =  UserDefaults.standard.string(forKey: "DeviceToken")
        let image = MyImage
        let myImgData = image!.jpegData(compressionQuality: 0.2)
        let ContactNumber = phoneNumberTF.text!
        let NewCpf = cpfTF.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        if userTypeView.text == "Vendedor"{
            parameter = ["email": eMailTF.text!,
                         "username" : userNameTF.text!,
                         "phone_number" : ContactNumber,
                         "address" : "\(mylocationId)",
                "brand" : "\(mybrandName)",
                "state" : "\(mystateName)",
                "city" :  "\(myCityName)",
                "password" : passwordTF.text!,
                "cpf_number": NewCpf,
                "device_type" : "ios",
                "device_token" : DeviceToken,
                "fcmtoken" : UserDefaults.standard.value(forKey: "FCM_TOKEN")as! String]
        }
        else{
            if userTypeView.text == "Comprador"{
                usertype = "2"
            }
            else if userTypeView.text == "Gerente"{
                usertype = "3"
            }
            parameter  = ["email": eMailTF.text!,
                          "username" : userNameTF.text!,
                          "phone_number" : ContactNumber,
                          "user_type" : usertype,
                          "user_Location" : LocationArray,
                          "password" : passwordTF.text!,
                          "profile_pic" : "",
                          "cpf_number": NewCpf,
                          "device_type" : "ios",
                          "device_token" : DeviceToken!,
                          "fcmtoken" : UserDefaults.standard.value(forKey: "FCM_TOKEN")as! String]
        }
        
        let methodname = methodsName.userCase.userSignUp.caseValue
        let url1 = BaseURL
        let url = ("\(url1)" + "\(methodname)")
        uploadImage1(urlString: url, params: parameter, imageKeyValue: "profile_pic", image: image, success: { (response) in
            print(response)
            let dict = response as Any
            self.hideProgress()
            if (dict as AnyObject).value(forKey: "success") as! Bool == true{
                if let data = (dict as AnyObject).value(forKey: "data") as? NSDictionary{
                    print(data)
                    
                    //Access API Token and save it
                    let usertoken = (dict as AnyObject).value(forKey: "token")
                    UserDefaults.standard.set(usertoken, forKey: "usertoken")
                    
                    // get user id nd save it
                    let userId = (data.value(forKey: "id") as! NSNumber)
                    UserDefaults.standard.set("\(userId)", forKey: "userid")
                    let GoHome = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                    self.navigationController?.pushViewController(GoHome, animated: true)
                }}
        })
            
        { (error) in
            print(error)
            
        }
    }
}
// MARK:- privacy policy and terms and conditions Tap Gesture
@available(iOS 11.0, *)
extension UITapGestureRecognizer{
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}

class LocationCollectionCell:UICollectionViewCell {
    
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var crossBtn: UIButton!
}

//MARK:-
@available(iOS 11.0, *)
extension SignInController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return LocationArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = locationCollectionView.dequeueReusableCell(withReuseIdentifier: "LocationCollectionCell", for: indexPath) as! LocationCollectionCell
        let locationDict = LocationArray[indexPath.row]
        cell.locationText.text = locationDict["Location"]
        cell.crossBtn.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cell = locationCollectionView.dequeueReusableCell(withReuseIdentifier: "LocationCollectionCell", for: indexPath) as! LocationCollectionCell
        let locationDict = LocationArray[indexPath.row]
        cell.locationText.text = locationDict["Location"]
        cell.locationText.sizeToFit()
        return CGSize(width: cell.locationText.frame.width + 25, height: 40)
    }
    
}



