//
//  MySpaceController.swift
//  Conselho Fashion
//
//  Created by apple on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
protocol profileViewDelegate: class {
    func addTabBarView(tap:Int)
}
@available(iOS 11.0, *)
class MySpaceController: UIViewController {
    
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var titleView1: UIView!
    @IBOutlet weak var titleView2: UIView!
    @IBOutlet weak var lable1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var pointsTable: UITableView!
    @IBOutlet weak var Earned: DesignableButton!
    @IBOutlet weak var Spend: DesignableButton!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var availablePointlbl: UILabel!
    @IBOutlet weak var pendingPointlbl: UILabel!
    @IBOutlet weak var mySpaceText: UILabel!
    @IBOutlet weak var availableText: UILabel!
    @IBOutlet weak var pendingPointText: UILabel!
    @IBOutlet weak var Redeemptionlbl: UILabel!
    @IBOutlet weak var Giftlbl: UILabel!
    @IBOutlet weak var costlbl: UILabel!
    @IBOutlet weak var pointslbl: UILabel!
    @IBOutlet weak var viewmoreBtn: UIButton!
    @IBOutlet weak var tableNodata: UILabel!
    
    
    weak var callDelegate : profileViewDelegate?
    var myprofileData : NSDictionary = [:]
    var isButtonSelected = "earned"
    var PointEarnedArray : NSArray = []
    var pointSpentArray : NSArray = []
    var mylanguageCode = "pt-BR"
    var LocationNameArray = [String]()
    var BrandNameArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Earned.backgroundColor = UIColor(named: "themeColor1")
        Earned.setTitleColor(UIColor.white, for: .normal)
        Spend.backgroundColor = CELL_SLECTED_COLOR
        Spend.setTitleColor(UIColor.lightGray, for: .normal)
        titleView1.isHidden = false
        titleView2.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        languageTranslate()
        self.navigationController?.isNavigationBarHidden = true
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        // Get User profile information Function Call
        getProfile()
        
    }
    //MARK:- language change
    func languageTranslate(){
        mySpaceText.text = "MySpace".localized(loc: mylanguageCode)
        viewmoreBtn.setTitle("VierMore".localized(loc: mylanguageCode), for: .normal)
        availableText.text = "AvailablePoints".localized(loc: mylanguageCode)
        pendingPointText.text = "PendingPoints".localized(loc: mylanguageCode)
        Earned.setTitle("pointEarn".localized(loc: mylanguageCode), for: .normal)
        Spend.setTitle("pointsSpent".localized(loc: mylanguageCode), for: .normal)
        Redeemptionlbl.text = "RedeemDate".localized(loc: mylanguageCode)
        Giftlbl.text = "Gift".localized(loc: mylanguageCode)
        costlbl.text = "Cost".localized(loc: mylanguageCode)
        pointslbl.text = "Points".localized(loc: mylanguageCode)
        lable1.text = "Points".localized(loc: mylanguageCode)
        label2.text = "Claimedon".localized(loc: mylanguageCode)
        label3.text = "Expireon".localized(loc: mylanguageCode)
        label4.text = "status".localized(loc: mylanguageCode)
    }
    
    // MARK:- Perform Action on viewMore button
    @IBAction func viewMore(_ sender: UIButton) {
        let gotoProfile = self.storyboard?.instantiateViewController(withIdentifier: "ProfileController") as! ProfileController
        gotoProfile.profileDictionary = myprofileData 
        self.navigationController?.pushViewController(gotoProfile, animated: true)
    }
    
    @IBAction func SettingTap(_ sender: UIButton) {
        let GotoSetting = self.storyboard?.instantiateViewController(withIdentifier: "SettingsController") as! SettingsController
        self.navigationController?.pushViewController(GotoSetting, animated: true)
        
    }
    
    @IBAction func EarnedBtn(_ sender: Any) {
        isButtonSelected = "earned"
        titleView1.isHidden = false
        titleView2.isHidden = true
        Earned.backgroundColor = UIColor(named: "themeColor1")
        Spend.backgroundColor = CELL_SLECTED_COLOR
        Earned.setTitleColor(UIColor.white, for: .normal)
        Spend.setTitleColor(UIColor.lightGray, for: .normal)
        pointsTable.reloadData()
    }
    @IBAction func SpendBtn(_ sender: UIButton) {
        isButtonSelected = "spend"
        titleView1.isHidden = true
        titleView2.isHidden = false
        Spend.backgroundColor = UIColor(named: "themeColor1")
        Earned.backgroundColor = CELL_SLECTED_COLOR
        Spend.setTitleColor(UIColor.white, for: .normal)
        Earned.setTitleColor(UIColor.lightGray, for: .normal)
        pointsTable.reloadData()
    }
    //MARK:- --------------------- Get profileData API----------------------------------------
    func getProfile(){
        showProgress()
        networkServices.shared.postDataWithoutParameter(methodname: methodsName.userCase.getProfile.caseValue) { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            print (dic)
            if let invalidtoken = dic.value(forKey: "message") as? String{
                if invalidtoken == "Invalid token."{
                    // Show alert View
                    let alertController = UIAlertController(title: "Conselho Fashion", message: "InvalidToken".localized(loc: self.mylanguageCode), preferredStyle: .alert)
                    let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                        UserDefaults.standard.removeObject(forKey: "userid")
                        UserDefaults.standard.removeObject(forKey: "usertoken")
                        // UserDefaults.standard.removeObject(forKey: "socialValue")
                        let Home = self.storyboard?.instantiateViewController(withIdentifier: "SignInController") as! SignInController
                        self.present(Home, animated:true, completion:nil)
                    }
                    alertController.addAction(acceptAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                else if dic.value(forKey: "success") as! Bool == true {
                    if let profileData = dic.value(forKey: "data") as? NSDictionary{
                        print(profileData)
                        // check user banned or not
                        if "\(profileData.value(forKey: "status") as! NSNumber)" == "0"{
                            // Show alert View for user banned
                            let alertController = UIAlertController(title: "Conselho Fashion", message: "Este usuário foi banido", preferredStyle: .alert)
                            let acceptAction = UIAlertAction(title: "OK", style: .default) { (_) -> Void in
                                UserDefaults.standard.removeObject(forKey: "userid")
                                UserDefaults.standard.removeObject(forKey: "usertoken")
                                let Home = self.storyboard?.instantiateViewController(withIdentifier: "SignInController") as! SignInController
                                self.present(Home, animated:true, completion:nil)
                            }
                            alertController.addAction(acceptAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else{
                            // Assign profileData dictionary into myprofileData varible
                            self.myprofileData = profileData
                            // get value from profileData nd put in to profile
                            if profileData.value(forKey: "profile_pic") as! String == "" {
                                self.profilePic.image = #imageLiteral(resourceName: "Asset 28")
                            }
                            else{
                                let profileImage = (profileData.value(forKey: "profile_pic") as! String)
                                let profileImageUrl = URL(string: profileImage)
                                if let profile = try? Data(contentsOf: profileImageUrl!)
                                {
                                    let image: UIImage = UIImage(data: profile)!
                                    self.profilePic.image = image
                                }
                            }
                            self.profileName.text = (profileData.value(forKey: "username") as! String)
                            
                            let userType = (profileData.value(forKey: "user_type") as! String)
                            SingletonClass.sharedInstance.usertype = userType
                            if userType == "1"{
                                
                                self.addressLbl.text = (profileData.value(forKey: "address_field") as? String)
                                self.brandName.text = (profileData.value(forKey: "brand") as? String)
                                SingletonClass.sharedInstance.BrandName = ((profileData.value(forKey: "brand") as! String))
                                SingletonClass.sharedInstance.locationId = ("\(profileData.value(forKey: "location_id") as! NSNumber)")
                            }
                            else{
                                self.LocationNameArray.removeAll()
                                self.BrandNameArray.removeAll()
                                SingletonClass.sharedInstance.locationIDArray.removeAll()
                                SingletonClass.sharedInstance.Newsletterarray.removeAll()
                                if let LocationArray = profileData.value(forKey: "user_Location") as? [[String:Any]]{
                                    for LocationDict in LocationArray{
                                        if let LocationName = LocationDict["Location"] as? String{
                                            self.LocationNameArray.append(LocationName)
                                        }
                                        if let BrandName = LocationDict["Brand"] as? String{
                                            self.BrandNameArray.append(BrandName)
                                          SingletonClass.sharedInstance.NewsletterDict.updateValue(BrandName, forKey: "Brand")
                                        }
                                        if let LocationId = LocationDict["LocationID"] as? String{
                                            SingletonClass.sharedInstance.locationIDArray.append(LocationId)
                                            SingletonClass.sharedInstance.NewsletterDict.updateValue(LocationId, forKey: "LocationID")
                                        }
                                         SingletonClass.sharedInstance.Newsletterarray.append(SingletonClass.sharedInstance.NewsletterDict)

                                        
                                    }
                                     print(SingletonClass.sharedInstance.Newsletterarray)
                                }
                                self.BrandNameArray.removeDuplicates()
                                self.LocationNameArray.removeDuplicates()
                                self.addressLbl.text = self.LocationNameArray.joined(separator: ",")
                                self.brandName.text = self.BrandNameArray.joined(separator: ",")
                            }
                            
                        }
                    }
                    self.pointDetails() //Get point Details API Call
                }
                else{
                    self.hideProgress()
                    self.ShowAlertView(title: "Conselho Fashion", message: dic.value(forKey: "message") as! String, viewController: self)
                }
            }
        }
    }
    
    
    
    
    
    
    //MARK:- PointDetail API
    func pointDetails(){
        networkServices.shared.getDataWithoutParameter(methodname: methodsName.userCase.MySpace.caseValue) { (response) in
            print(response)
            let dic = response as! NSDictionary
            if  dic.value(forKey: "success") as! Bool == true{
                let data = dic.value(forKey: "data") as? NSDictionary
                print(data)
                self.availablePointlbl.text = ("\(data?.value(forKey: "available_points") as! NSNumber)")
                self.pendingPointlbl.text = ("\(data?.value(forKey: "pending_points") as! NSNumber)")
                self.PointEarnedArray = data?.value(forKey: "pointsEarned") as! NSArray
                self.pointSpentArray = data?.value(forKey: "pontSpent") as! NSArray
                self.pointsTable.delegate = self
                self.pointsTable.dataSource = self
                self.pointsTable.reloadData()
            }
        }
        
    }
    
}

@available(iOS 11.0, *)
extension MySpaceController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isButtonSelected == "earned"{
            tableNodata.isHidden = PointEarnedArray.count == 0 ? false : true
            return PointEarnedArray.count
        }
        else{
            tableNodata.isHidden = pointSpentArray.count == 0 ? false : true
            return pointSpentArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isButtonSelected == "earned" {
            let myEarnedData = PointEarnedArray[indexPath.row] as! NSDictionary
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as? mySpaceCell{
                if indexPath.row % 2 == 0{
                    cell.bgView.backgroundColor = UIColor.white
                }else{
                    cell.bgView.backgroundColor = CELL_SLECTED_COLOR
                }
                cell.pointslbl.text = (myEarnedData.value(forKey: "points") as! String)
                cell.claimedOnlbl.text = (myEarnedData.value(forKey: "claimed_on") as! String)
                cell.expireONlbl.text = (myEarnedData.value(forKey: "expires_on") as! String)
                cell.statuslbl.text = (myEarnedData.value(forKey: "status") as! String)
                return cell
            }
        }else{
            let mySpendData = pointSpentArray[indexPath.row] as! NSDictionary
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as? mySpaceCell1{
                if indexPath.row % 2 == 0{
                    cell.bgView.backgroundColor = UIColor.white
                }else{
                    cell.bgView.backgroundColor = CELL_SLECTED_COLOR
                }
                cell.redeemDate.text = (mySpendData.value(forKey: "redeemption_date") as! String)
                cell.GiftCardName.text = (mySpendData.value(forKey: "gift_card") as! String)
                cell.cardCost.text = (mySpendData.value(forKey: "cost") as! String)
                cell.pointslbl.text = "\(mySpendData.value(forKey: "points") as! NSNumber)"
                return cell
            }
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isButtonSelected == "earned" {
            return 30
        }
        else{
            return 50
        }
    }
    
}
class mySpaceCell:UITableViewCell{
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var pointslbl: UILabel!
    @IBOutlet weak var claimedOnlbl: UILabel!
    @IBOutlet weak var expireONlbl: UILabel!
    @IBOutlet weak var statuslbl: UILabel!
    
    
}
class mySpaceCell1:UITableViewCell{
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var redeemDate: UILabel!
    @IBOutlet weak var GiftCardName: UILabel!
    @IBOutlet weak var cardCost: UILabel!
    @IBOutlet weak var pointslbl: UILabel!
    
}
extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}
