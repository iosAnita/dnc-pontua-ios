//
//  RedeemPricesController.swift
//  KitoPlastic
//
//  Created by apple on 06/05/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
class RedeemPricesController: UIViewController {
@IBOutlet weak var NavigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var GiftCollectionView: UICollectionView!
    @IBOutlet weak var AwardRedeemlbl: UILabel!
    
    var giftImages = [UIImage]()
    var MyGiftDetail : [Any] = []
    var mylanguageCode = "pt-BR"
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavigationBarImage()
        self.navigationController?.isNavigationBarHidden = false
        giftImages = [UIImage(named: "G1"),UIImage(named: "G2"),UIImage(named: "G3"),UIImage(named: "G4"),UIImage(named: "G5"),UIImage(named: "G1")] as! [UIImage]
        let screenSize = UIScreen.main.bounds
        if screenSize.height <= 736{
            NavigationViewHeight.constant = 64
        }else{
            NavigationViewHeight.constant = 88
        }
         GetVouncherList() //API Call
        // Do any additional setup after loading the view.
    }
    
    @IBAction func ReddemBtnTap(_ sender: UIButton) {
        let selectedButton = sender.tag
        SingletonClass.sharedInstance.GiftVouncherDetail = MyGiftDetail[selectedButton] as! NSDictionary
        self.performPushSeguefromController(identifier: "GiftCardDetailController")
        }
    
    override func viewWillAppear(_ animated: Bool) {
      
        languagechange()
    }
    //MARK:- languageChange
   func languagechange(){
    AwardRedeemlbl.text = "AwardRedemption".localized(loc: mylanguageCode)
    }
    
    //MARK:- Get Gift Vouncher list
    func GetVouncherList(){
        self.showProgress()
        networkServices.shared.getDataWithoutParameter(methodname: methodsName.userCase.GiftVouncherList.caseValue) { (response) in
            print(response)
            self.hideProgress()
            let dic = response as! NSDictionary
            if dic.value(forKey: "success") as! Bool == true{
                if let data = dic.value(forKey: "data") as? NSArray{
                        // Asiign data in MyGiftDetail variable
                    self.MyGiftDetail = data as! [Any]
                    // Reload Collection View
                    self.GiftCollectionView.delegate = self
                    self.GiftCollectionView.dataSource = self
                    self.GiftCollectionView.reloadData()
                 }
            }
         }
    }

}
@available(iOS 11.0, *)
extension RedeemPricesController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return MyGiftDetail.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let VouncherData = MyGiftDetail[indexPath.row] as! NSDictionary
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? giftCell{
            cell.giftImg.layer.cornerRadius = 10.0
            cell.giftImg.clipsToBounds = true
//            cell.ReddemBtn.setTitle("Redeem".localized(loc: mylanguageCode), for: .normal)
            cell.GiftTittle.text = "CAModes".localized(loc: mylanguageCode)
          //  cell.reedemImg.layer.cornerRadius = 10.0
           // cell.reedemImg.clipsToBounds = true
            cell.ReddemBtn.tag = indexPath.row
            if (VouncherData.value(forKey: "image") as! String == "") {
                cell.giftImg.image = #imageLiteral(resourceName: "placeholder-image1")
            }
            else{
                // change image URL in to UIImage Type
                if let endPoint = VouncherData.value(forKey: "image")  as? String, let profileimage = endPoint.configUrl() {
                    cell.giftImg.sd_setImage(with: profileimage, placeholderImage: #imageLiteral(resourceName: "placeholder-image1"))
                } else {
                    cell.giftImg.image = UIImage(named: "placeholder-image1")
                }
            }
            cell.GiftPriceRange.text = (VouncherData.value(forKey: "price_range") as! String)
            cell.ValidityTime.text = (VouncherData.value(forKey: "validity_period") as! String)
            // Seprate Tittle String
            let str = VouncherData.value(forKey: "title") as! String
//            if let range = string.range(of: "-") {
//                let firstPart = string[(string.startIndex)..<range.lowerBound]
//                print(firstPart)
//                cell.GiftTittle.text = (firstPart as! String)
//            }
            let TittleString = str.components(separatedBy: "-")
            let NetTittle = TittleString[0]
            cell.GiftTittle.text = NetTittle
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(collectionView.frame.width)/2-5, height: 270)
    }
    
}
class giftCell:UICollectionViewCell{
    
    @IBOutlet weak var reedemImg: DesignableImage!
    @IBOutlet weak var giftImg: UIImageView!
    @IBOutlet weak var viewBG: DesignableView!
    @IBOutlet weak var GiftTittle: UILabel!
    @IBOutlet weak var GiftPriceRange: UILabel!
    @IBOutlet weak var ValidityTime: UILabel!
    @IBOutlet weak var ReddemBtn: UIButton!
}
